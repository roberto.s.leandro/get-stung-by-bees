  var watchExampleVM = new Vue({
    el: '#watch-example',
    data: {
      question: '',
      imageLocation: '',
      errorMessage: 'YOU GOTTA INCLUDE A QUESTION MARK YOU TURTLE!!!!',
      showPicture: false,
      showError: false,
    },
    watch: {
      // whenever question changes, this function will run
      question: function (newQuestion, oldQuestion) {
        this.answer = 'Waiting for you to stop typing...'
        this.debouncedGetAnswer()
      }
    },
    created: function () {
      // _.debounce is a function provided by lodash to limit how
      // often a particularly expensive operation can be run.
      // In this case, we want to limit how often we access
      // yesno.wtf/api, waiting until the user has completely
      // finished typing before making the ajax request. To learn
      // more about the _.debounce function (and its cousin
      // _.throttle), visit: https://lodash.com/docs#debounce
      this.debouncedGetAnswer = _.debounce(this.getAnswer, 50)
    },
    methods: {
      getAnswer: function () {
        this.showError = false
        this.showPicture = false

        if(this.question === ''){
          return 
        }

        if (this.question.indexOf('?') === -1 && this.question != ''){
          this.showError = true

          return
        }
        var vm = this
        axios.get('https://yesno.wtf/api')
          .then(function (response) {
            vm.imageLocation = response.data.image
            vm.showPicture = true
          })
          .catch(function (error) {
            vm.answer = 'Error! Could not reach the API. ' + error
          })

      }
    }
  })